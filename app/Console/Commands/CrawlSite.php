<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Event;
use App\Events\ReadyForCrawl;
use Carbon\Carbon;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class CrawlSite extends Command implements ShouldQueue
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:crawl';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        print(Carbon::now() . ' * CrawlSite' . PHP_EOL);
        $rate = config('app.crawlrate');
        $pages = \App\SiteLog::crawled(false)->oldest()->take($rate)->get();
        print(Carbon::now() . ' | ' . $pages->count() . PHP_EOL);
        foreach ($pages as $page) {
            print(Carbon::now() . ' | Queued ' . $page->url . PHP_EOL);
            Event::fire(new ReadyForCrawl($page));
        }
    }
}
