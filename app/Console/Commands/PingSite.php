<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Event;
use App\Events\ReadyForPing;

class PingSite extends Command implements ShouldQueue
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'site:ping';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Checks if a site is running and stores the responsetime';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach (\App\Site::all() as $site) {
            Event::fire(new ReadyForPing($site));
        }
    }
}
