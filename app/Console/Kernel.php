<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\PingSite::class,
        \App\Console\Commands\CrawlSite::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')
                 ->hourly();
        $schedule->command('site:ping')
                 ->everyThirtyMinutes()
                 ->appendOutputTo(storage_path('ping.txt'));
        $schedule->command('site:crawl')
                 ->everyMinute()
                 ->appendOutputTo(storage_path('crawl.txt'));
    }
}
