<?php

namespace App\Listeners;

use App\Events\ReadyForPing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon; 

class CheckAnalytics implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReadyForPing  $event
     * @return void
     */
    public function handle(ReadyForPing $event)
    {
        $site = \App\Site::find($event->site->id);
        $response = get_headers($site->url);
        if ($response) {
            $content = file_get_contents($site->url, false);
            if (str_contains($content, "googletagmanager.com/gtm.js") || str_contains($content, "var _gaq = _gaq") || str_contains($content, "['GoogleAnalyticsObject']") || str_contains($content, "googletagservices.com/tag/js/gpt.js")) {
                $site->update(['analytics'=>true]);
                print(Carbon::now() . ' | Found analytics for ' . $site->url . PHP_EOL);
            } else {
                $site->update(['analytics'=>false]);
                print(Carbon::now() . ' | No analytics for ' . $site->url . PHP_EOL);
            }
        }
    }
}
