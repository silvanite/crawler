<?php

namespace App\Listeners;

use App\Events\ReadyForCrawl;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class Crawl implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReadyForCrawl  $event
     * @return void
     */
    public function handle(ReadyForCrawl $event)
    {
        print(Carbon::now() . ' * Crawl' . PHP_EOL);
        // crawl page
        $site = \App\SiteLog::find($event->page->id);
        $start = microtime(true);
        $response = get_headers($site->url);
        $responseTime = round(microtime(true) - $start,3)*1000;
        // store result
        if ($response) {
            $site->update(['url'=>$site->url, 'responseCode'=>substr($response[0], 9, 3), 'responseTime'=>$responseTime,'crawled'=>true]);
            print(Carbon::now() . ' | Pinging ' . $site->url . ' (' . $responseTime . 'ms)' . PHP_EOL);
        }
    }
}
