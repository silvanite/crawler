<?php

namespace App\Listeners;

use App\Events\ReadyForPing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class ParseSitemap
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReadyForPing  $event
     * @return void
     */
    public function handle(ReadyForPing $event)
    {
        print(Carbon::now() . ' * ParseSitemap' . PHP_EOL);
        $site = \App\Site::find($event->site->id);
        $head = get_headers($site->url . '/robots.txt');
        if ($head && substr($head[0], 9, 3) == '200') {
            $robots = file_get_contents($site->url . '/robots.txt', false);
            $parser = new \webignition\RobotsTxt\File\Parser();
            $parser->setSource($robots);
            
            $file = $parser->getFile();
            
            $sitemapDirectives = $file->directiveList()->filter(array('field' => 'sitemap'))->get();
            libxml_use_internal_errors(TRUE);
            foreach ($sitemapDirectives as $sitemapentry) {
                    $sitemapurl = $sitemapentry ? $sitemapentry->getValue() : null;
                    $sitemap = $sitemapentry ? file_get_contents($sitemapurl) : null;
                    $xml = $sitemap ? simplexml_load_string($sitemap) : false;
                
                if ($xml === false) {
                    $site->update(['sitemap'=>false]);
                    foreach(libxml_get_errors() as $error) {
                        // echo($error->message);
                    }
                } else {
                    $site->update(['sitemap'=>true]);
                    if ($xml->sitemap->count()) {
                        // multiple sitemaps
                        foreach ($xml->sitemap as $sitemapinstance) {
                            $sitemapurl = $sitemapinstance->loc;
                            $sitemap = $sitemapentry ? file_get_contents($sitemapurl) : null;
                            $xml = $sitemap ? simplexml_load_string($sitemap) : false;
                            if ($xml) {
                                $count = $xml->url->count();
                                foreach ( $xml->url as $page ) {
                                    // delete last logged page
                                    $site->logs()->where('url', $page->loc)->delete();
                                    // add page to sitelog for crawling
                                    $site->logs()->create(['url'=>$page->loc]);
                                }
                                print(Carbon::now() . ' | Sitemap found ' . $sitemapurl . " ($count)" . PHP_EOL);
                            }
                        }
                    } else {
                        // single sitemap
                        $count = $xml->url->count();
                        foreach ( $xml->url as $page ) {
                            // delete last logged page
                            $site->logs()->where('url', $page->loc)->delete();
                            // add page to sitelog for crawling
                            $site->logs()->create(['url'=>$page->loc]);
                        }
                        print(Carbon::now() . ' | Sitemap found ' . $sitemapurl . " ($count)" . PHP_EOL);
                    }
                }
            }
            
        } else {
            $site->update(['sitemap'=>false]);
            print(Carbon::now() . ' | No Sitemap ' . $site->url . PHP_EOL);
        }
    }
}
