<?php

namespace App\Listeners;

use App\Events\ReadyForPing;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Carbon\Carbon;

class PingSite implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ReadyForPing  $event
     * @return void
     */
    public function handle(ReadyForPing $event)
    {
        print(Carbon::now() . ' * PingSite' . PHP_EOL);
        $site = \App\Site::find($event->site->id);
        $start = microtime(true);
        $response = get_headers($site->url);
        $responseTime = round(microtime(true) - $start,3)*1000;
        // store result in site logs
        if ($response) {
            $site->logs()->where('url', $site->url)->delete();
            $site->logs()->create(['url'=>$site->url, 'responseCode'=>substr($response[0], 9, 3), 'responseTime'=>$responseTime,'crawled'=>true]);
            print(Carbon::now() . ' | Pinging ' . $site->url . ' (' . $responseTime . 'ms)' . PHP_EOL);
        }
    }
}
