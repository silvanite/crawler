<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Site extends Model
{
    // setup mass assignment
    protected $fillable = [
            'url',
            'sitemap',
            'analytics',
            'responsetime',
            'pagesup'
        ];
        
    public function logs()
    {
        return $this->hasMany(\App\SiteLog::class);
    }
    
    public function getAvgResponseTime()
    {
        return (int)$this->logs()->crawled()->latest()->withSuccess()->take(100)->avg('responsetime');
    }
    
    public function getPagesUp()
    {
        return (int)$this->logs()->crawled()->withSuccess()->count();
    }
    
    public function getPagesDown()
    {
        return (int)$this->logs()->crawled()->withError()->count();
    }
}
