<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SiteLog extends Model
{
    protected $fillable = [
            'site_id',
            'url',
            'responseCode',
            'responseTime',
            'crawled'
        ];
        
    public function site()
    {
        return $this->belongsTo(\App\Site::class);
    }
    
    public function scopeCrawled($query, $status = true)
    {
        return $query->where('crawled', $status);
    }
    
    public function scopeWithCode($query, $code = 200)
    {
        return $query->where('responseCode', $code);
    }
    
    public function scopeWithSuccess($query)
    {
        return $query->whereIn('responseCode', [200,301,302]);
    }
    
    public function scopeWithError($query)
    {
        return $query->whereNotIn('responseCode', [200,301,302]);
    }
    
    public function scopeLatest($query)
    {
        return $query->orderBy('created_at','desc');
    }
    
    public function scopeOldest($query)
    {
        return $query->orderBy('created_at','asc');
    }
}
