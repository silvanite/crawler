# SEO Crawler

Framework: Laravel PHP [![Build Status](https://travis-ci.org/laravel/framework.svg)](https://travis-ci.org/laravel/framework)
[![Latest Stable Version](https://poser.pugx.org/laravel/framework/v/stable.svg)](https://packagist.org/packages/laravel/framework)
[![License](https://poser.pugx.org/laravel/framework/license.svg)](https://packagist.org/packages/laravel/framework)

This software is intended as a server side seo crawler to monitor the status of a group of website and check them for a number of different criteria.

* Check site response time
* Look for sitemap in robots txt
* Index / ping all pages within the sitemap(s)
* Basic checking for Google analytics tag on homepage (and/or google tag manager)

The admin interface is REST compatible and allows for adding, removing and viewing sites and their last 100 log entiries.

## Automation

A single cron job executes the laravel scheduling system every minute. Within the application the following tasks are scheduled:

* Every minute: Crawl up to 100 pages
* Every 30 minutes: Ping sites and parse sitemaps. Queue pages to be crawled.

## To-do

* Admin authentication
* REST authentication
* Improve table layout on mobile devices
* Add unit tests
* Add seeding for testing
* Edit site functionality
* Improve event listeners (repetitive coding)
* Implement laravel queues and jobs instead of looping through database
* Clean up JavaScript and move to external js file.
* Break-up blade tempaltes into smaller chunks

## Development notes

The system currently uses two linked ORM models, Site.php and SiteLog.php. Each Site can have many SiteLog and each SiteLog belongs to one Site.

Database schema is located in the migrations \database\migrations. No seeding or factories are defined. 

Rest api is handled by \app\Http\Controllers\SiteController.php and \app\Http\routes.php

Artisan commands and schedules are defined in \app\Console\Kernel.php

There are 2 "Events" (\app\Events) defined, ReadyForCrawl and ReadyForPing, 
both of which can and do have "Listeners" (\app\Listeners) which get executed when the event is fired.

The system is using CDN bootstrap 3 to speed up development. There is some javascript code within the blade tempalte to create DELETE requests from anchor links.
All views are in \resources\views with one master view which is extended by the page views which include elements/partial views.