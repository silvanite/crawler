@extends('master.bootstrap')

@section('body')
    <div class="row">
        <div class="col-md-12">
            <h4>Logs</h4>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Time</th>
                        <th>URL</th>
                        <th>Status</th>
                        <th>Ping</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($site->logs->take(100) as $log)
                    <tr>
                        <td>{{ $log->created_at }}</td>
                        <td>{{ $log->url }}</td>
                        <td>{{ $log->responseCode }}</td>
                        <td>{{ $log->responseTime }}</td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>

@endsection