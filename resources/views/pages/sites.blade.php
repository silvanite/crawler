@extends('master.bootstrap')

@section('body')
    <div class="row">
        <div class="col-md-12">
            <h4>Sites</h4>
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Site</th>
                        <th>Sitemap</th>
                        <th>Analytics</th>
                        <th>Pages</th>
                        <th class="hidden-xs">Avg response time (ms)</th>
                        <th class="text-right hidden-xs">Actions</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (\App\Site::all() as $site)
                    <tr>
                        <td>{{ $site->url }}</td>
                        <td><span class="glyphicon {{ $site->sitemap ? 'glyphicon-ok text-success' : 'glyphicon-remove text-danger' }}" aria-hidden="true"></span></td>
                        <td><span class="glyphicon {{ $site->analytics ? 'glyphicon-ok text-success' : 'glyphicon-remove text-danger' }}" aria-hidden="true"></span></td>
                        <td><span class="label label-success">{{ $site->getPagesUp() }}</span> <span class="label label-danger">{{ $site->getPagesDown() }}</span></td>
                        <td class="hidden-xs">
                            <div class="progress">
                              <div class="progress-bar" role="progressbar" aria-valuenow="{{ $site->getAvgResponseTime() }}" aria-valuemin="0" aria-valuemax="1000" style="min-width: 2em; width: {{ $site->getAvgResponseTime() / 1000 * 100 }}%;">{{ $site->getAvgResponseTime() }}</div>
                            </div>
                        </td>
                        <td class="text-right hidden-xs">
                            <div class="btn-group" role="group" aria-label="...">
                                <a class="btn btn-primary btn-sm" href="{{ route('site.show',['id'=>$site->id]) }}"><span class="glyphicon glyphicon-search" aria-hidden="true"></span></a>
                                <button class="btn btn-danger btn-sm" data-href="{{ route('site.destroy',['id'=>$site->id]) }}" data-toggle="modal" data-target="#confirm"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
            <p>
                <!-- Button trigger modal -->
                <button type="button" class="btn btn-primary btn-md" data-toggle="modal" data-target="#myModal">
                  Add site
                </button>
            </p>
        </div>
    </div>
    
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog">
        <form action="{{ route('site.store') }}" method="post">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Add a site</h4>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label sr-only" for="url">Website URL</label>
                            <div class="input-group">
                                <span class="input-group-addon"><span class="glyphicon glyphicon-globe" aria-hidden="true"></span></span>
                                <input type="text" class="form-control" name="url" id="url" placeholder="http://www.dewsign.co.uk">
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                        {{ csrf_field() }}
                    </div>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </form>
    </div>
    <!-- /.modal -->
    
    <div class="modal fade" id="confirm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    Delete site?
                </div>
                <div class="modal-body">
                    Are you sure you want to delete this site and all associated logs?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <a class="btn btn-danger btn-ok" data-method="DELETE">Delete</a>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
    @parent
    
    <script>
        window.csrfToken = '{{ csrf_token() }}';
        
        (function() {
            var laravel = {
                initialize: function() {
                    this.registerEvents();
                },
        
                registerEvents: function() {
                    $('body').on('click', 'a[data-method]', this.handleMethod);
                },
        
                handleMethod: function(e) {
                    var link = $(this);
                    var httpMethod = link.data('method').toUpperCase();
                    var form;
        
                    // If the data-method attribute is not PUT or DELETE,
                    // then we don't know what to do. Just ignore.
                    if ( $.inArray(httpMethod, ['PUT', 'DELETE']) === - 1 ) {
                        return;
                    }
        
                    // Allow user to optionally provide data-confirm="Are you sure?"
                    if ( link.data('confirm') ) {
                        if ( ! laravel.verifyConfirm(link) ) {
                            return false;
                        }
                    }
        
                    form = laravel.createForm(link);
                    form.submit();
        
                    e.preventDefault();
                },
        
                verifyConfirm: function(link) {
                    return confirm(link.data('confirm'));
                },
        
                createForm: function(link) {
                    var form =
                        $('<form>', {
                            'method': 'POST',
                            'action': link.attr('href')
                        });
        
                    var token =
                        $('<input>', {
                            'name': '_token',
                            'type': 'hidden',
                            'value': window.csrfToken
                        });
        
                    var hiddenInput =
                        $('<input>', {
                            'name': '_method',
                            'type': 'hidden',
                            'value': link.data('method')
                        });
        
                    return form.append(token, hiddenInput)
                        .appendTo('body');
                }
            };
        
            laravel.initialize();
        
        })();
        
        $( document ).ready(function() {
            $('#confirm').on('show.bs.modal', function(e) {
                $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
            });
        });
    </script>
@endsection